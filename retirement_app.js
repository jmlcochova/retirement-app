function getUserYearOfBirth(userInput) {
	return Number(userInput);
}

function getPresentYear() {
	return new Date().getFullYear();
}

function isInputYearInFuture(userInput) {
	return getUserYearOfBirth(userInput) > getPresentYear() ? true : false;
}

function getUserBirthDate() {
	let userInput;

	do {
		if (userInput !== undefined) {
			const userYearOfBirth = getUserYearOfBirth(userInput);

			if (userInput === "") {
				alert("Nezadali jste žádnou hodnotu. Prosím zadejte váš rok narození.")
			} else if (isNaN(userYearOfBirth)) {
				alert("Nezadali jste číslo. Zadejte prosím číslo.");
			} else if (userYearOfBirth % 1 !== 0 || userYearOfBirth < 0) {
				alert("Zadali jste neplatné číslo. Zadejte prosím celé kladné číslo o 4 číslicích.");
			} else if (isInputYearInFuture(userInput)) {
				alert("Zadali jste rok, který je v budoucnu. Prosím vyplňte váš reálný rok narození.");
			} else {
				alert("Pokud nejste novým rekordmanem a tím pádem nejstrarším člověkem planety, prosím zkontrolujte, že jste vyplnily správně svůj rok narození.");
			}
		}

		userInput = prompt("Prosím, vyplňte svůj rok narození.");
	} while (!isUserInputValid(userInput));

	if (isUserInputValid(userInput)) {
		return getUserAge(userInput);
	}
}

function getUserAge(userInput) {
	const userYearOfBirth = getUserYearOfBirth(userInput);
	const presentYear = new Date().getFullYear();

	return presentYear - userYearOfBirth;
}

function isUserInputValid(userInput) {
	const userAge = getUserAge(userInput);
	const userYearOfBirth = getUserYearOfBirth(userInput);
	const futureYear = isInputYearInFuture(userInput);
	const tooOld = userAge > 130;
	const emptyInput = userInput === "";
	const notANumber = isNaN(userYearOfBirth);
	const notAValidNumber = (userYearOfBirth % 1 !== 0 || userYearOfBirth < 0);
	const invalidInput = emptyInput || notANumber || notAValidNumber || futureYear || tooOld;

	return invalidInput ? false : true;
}

function isUserInPension(age) {
	const retirementAge = age >= 65;

	return retirementAge ? true : false;
}

function evaluatePensionStatus() {
	const age = getUserBirthDate();

	if (isUserInPension(age)) {
		alert(`Blahopřejeme, jste již v důchodu :).`);
	} else {
		alert(`Nebojte, v důchodu budete dřív, než si myslíte :P.`);
	}
} 
